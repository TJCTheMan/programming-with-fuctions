Please respond thoroughly (at least two sentences for each question) to
the following three questions.

1. Describe an experience you had this semester where you saw the power of
functions.

This semester really helped me to understand the purpose of functions and how useful they are. As I went through this semester, I wrote a Discord Bot that, using the same API I used for my final project, checks the status of Minecraft servers and responds with the status. I had been struggling finding a way to make this bot do what i wanted, but with the power of a couple functions, it was made easy.




2. In your judgment, what is the value of test functions?
Test functions allow you, the programmer, to verify that your functions work as intended, across many different scenarios and inputs. This is very useful to find errors, help with debugging, etc. Without this, you basically have to run your program over and over with different inputs, leading to a lot of trial and error. Test functions really help cut down on trial and error, as well as save time and effort.



3. Describe your process for troubleshooting and fixing a program that
has a problem. What is one thing you can do to improve your process?

If a program doesn't run correctly, the first thing I do, is read the output, see if there are specified errors, and fix them. If that fails, I set a breakpoint at the first function call in the program, and go through the debug system, making sure my variables get set correctly, making sure there arent any obvious punctuation errors, etc. If i still can't figure it out, back to the documentation! Read the documentation on the modules you are using, read basic python docs, google, etc. Between all this and the test file, I have (so far) gotten eveything to do what I want.



