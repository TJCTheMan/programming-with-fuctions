# Copyright Tevin Coles 2023 CSE111

"""
This program reads the contents
of students.csv, stores numbers, and checks input against them.

"""
import csv

def main():

    prod_dict = read_dictionary('/home/tj/school/programming-with-functions/w9/products.csv')
    
    print(f"\nAll Products: \n {prod_dict} \n \nRequested Items: ")

    with open('/home/tj/school/programming-with-functions/w9/request.csv', 'rt') as order:
        order_csv = csv.reader(order)
        next(order_csv)

        for row_list in order_csv:
            key = row_list[0]
            quantity = row_list[1]

            product = (prod_dict[key])[1]
            price = (prod_dict[key])[2]

            print(f"{product}: {quantity} @ ${price}")

    



    

def read_dictionary(filename):
    """Read the contents of a CSV file into a
    dictionary and return the dictionary.

    Parameters
        filename: the name of the CSV file to read.
    Return: a dictionary that contains
        the contents of the CSV file.
    """

    outputdict = {}

    with open(filename, 'rt') as file:
        file_csv = csv.reader(file)
        next(file_csv)

        for row_list in file_csv:
            if len(row_list) != 0:
                key = row_list[0]
                outputdict[key] = row_list

    return outputdict

if __name__ == "__main__":
    main()
