# Copyright Tevin Coles 2023 CSE111

"""
This program reads the contents
of students.csv, stores numbers, and checks input against them.

"""
import csv

def main():

    list_dict = read_dictionary('/home/tj/school/programming-with-functions/w9/students.csv')

    # print(list_dict)
    # print (list_dict['052058203'])

    i_num = input(f"Input an I number without dashes. (eg. 123456789): ")
    

    if i_num in list_dict:
        print((list_dict[i_num])[1])
    else:
        print("No such student.")

def read_dictionary(filename):
    """Read the contents of a CSV file into a
    dictionary and return the dictionary.

    Parameters
        filename: the name of the CSV file to read.
    Return: a dictionary that contains
        the contents of the CSV file.
    """

    outputdict = {}

    with open(filename, 'rt') as file:
        file_csv = csv.reader(file)
        next(file_csv)

        for row_list in file_csv:
            if len(row_list) != 0:
                key = row_list[0]
                outputdict[key] = row_list

    return outputdict

if __name__ == "__main__":
    main()
