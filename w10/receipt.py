# Copyright Tevin Coles 2023 CSE111

"""
This program reads the contents
of students.csv, stores numbers, and checks input against them.

"""
import csv

# Import the datetime class from the datetime
# module so that it can be used in this program.
from datetime import datetime

# Call the now() method to get the current
# date and time as a datetime object from
# the computer's operating system.
current_date_and_time = datetime.now()





def main():
    try:
        num_products = 0
        subtotal = 0
        prod_dict = read_dictionary('products.csv')
        store_name = f"The Inkom Emporium"

        print(f"{store_name}\n")
        
        # print(f"Requested Items: ")
        

        with open('request.csv', 'rt') as order:
            order_csv = csv.reader(order)
            next(order_csv)

            for row_list in order_csv:
                key = row_list[0]
                quantity = row_list[1]

                prod_key = prod_dict[key]

                product = prod_key[1]
                price = prod_key[2]
                num_products += int(quantity)
                subtotal += (float(price) * int(quantity))

                


                print(f"{product}: {quantity} @ ${price}")

        discount_amount = 0
        # if True:
        if current_date_and_time.weekday() == (1 or 2):
            print(f"It's {current_date_and_time.strftime('%A')}, you got a discount!")
            discount_amount += subtotal * 0.1
            subtotal = subtotal * 0.9
        
        if int(current_date_and_time.strftime('%H'))< 11:
            print(f"It's before 11 AM, you got an Early Bird discount!")
            discount_amount += subtotal * 0.1
            subtotal = subtotal * 0.9

        sales_tax = subtotal * 0.06
        total = subtotal + sales_tax






        print(f"\nNumber of items: {num_products}\n")
        print(f"Subtotal: ${subtotal:.2f}")
        print (f"Discount: ${discount_amount:.2f}")
        print(f"Sales Tax: ${sales_tax:.2f}")
        print(f"Total: ${total:.2f}")

        print(f"\nThank you for shopping at {store_name}")

        print(f"{current_date_and_time:%a %b %d %I:%M:%S %Y}")


    except KeyError as key_err:
        print(f"\nError: unknown product ID in the request.csv file: {key_err}")
        

    except FileNotFoundError as fnf_err:
        print(f"\n{fnf_err}.")



    

def read_dictionary(filename):
    """Read the contents of a CSV file into a
    dictionary and return the dictionary.

    Parameters
        filename: the name of the CSV file to read.
    Return: a dictionary that contains
        the contents of the CSV file.
    """

    outputdict = {}

    with open(filename, 'rt') as file:
        file_csv = csv.reader(file)
        next(file_csv)

        for row_list in file_csv:
            if len(row_list) != 0:
                key = row_list[0]
                outputdict[key] = row_list

    return outputdict

if __name__ == "__main__":
    main()
