


def main():
    try:
        # Create and print a list named fruit.
        fruit_list = ["pear", "banana", "apple", "mango"]
        print(f"Original: {fruit_list}")
        fruit_list.reverse()
        print(f"Reversed: {fruit_list}")
        fruit_list.append("orange")
        print(f"Orange appended: {fruit_list}")
        loc_apple = fruit_list.index("apple")
        fruit_list.insert(loc_apple, "cherry")
        print(f"Cherry Added: {fruit_list}")
        fruit_list.pop(fruit_list.index("banana"))
        print(f"Removed Banana: {fruit_list}")
        last = fruit_list.pop()
        print(f"Popped {last}: {fruit_list}")
        fruit_list.sort()
        print(f"Sorted: {fruit_list}")
        fruit_list.clear()
        print(f"Cleared: {fruit_list}")

    except IndexError as index_err:
        print(type(index_err).__name__, index_err, sep=": ")
    


if __name__ == "__main__":
    main()