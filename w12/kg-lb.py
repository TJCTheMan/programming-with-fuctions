# Copyright 2023, Tevin Coles. 

import tkinter as tk
from tkinter import Frame, Label, Button
from number_entry import IntEntry


def main():
    # Create the Tk root object.
    root = tk.Tk()

    # Create the main window. In tkinter,
    # a window is also called a frame.
    frm_main = Frame(root)
    frm_main.master.title("Heart Rate")
    frm_main.pack(padx=4, pady=3, fill=tk.BOTH, expand=1)

    # Call the populate_main_window function, which will add
    # labels, text entry boxes, and buttons to the main window.
    populate_main_window(frm_main)

    # Start the tkinter loop that processes user events
    # such as key presses and mouse button clicks.
    root.mainloop()



def populate_main_window(frm_main):
    """Populate the main window of this program. In other words, put
    the labels, text entry boxes, and buttons into the main window.

    Parameter
        frm_main: the main frame (window)
    Return: nothing
    """
    # Create a label that displays "Wieght in KG: "
    lbl_kg = Label(frm_main, text="Weight in KG: ")

    # Create an integer entry box where the user will enter the weight in KG
    ent_kg = IntEntry(frm_main, width=4, lower_bound=0)

    # Create a label that displays "KG"
    lbl_kg_units = Label(frm_main, text="KG")

    # Create a label that displays "Pounds:"
    lbl_lbs = Label(frm_main, text="Pounds:")

    # Create labels that will display the results.
    
    lbl_lbs_out = Label(frm_main)
    lbl_rate_units = Label(frm_main, text="LBS")

    # Create the Clear button.
    btn_clear = Button(frm_main, text="Clear")

    # Layout all the labels, entry boxes, and buttons in a grid.
    lbl_kg.grid(      row=0, column=0, padx=3, pady=3)
    ent_kg.grid(      row=0, column=1, padx=3, pady=3)
    lbl_kg_units.grid(row=0, column=2, padx=0, pady=3)

    lbl_lbs.grid(     row=1, column=0, padx=(30,3), pady=3)
    lbl_lbs_out.grid(      row=1, column=2, padx=3, pady=3)
    lbl_rate_units.grid(row=1, column=3, padx=0, pady=3)

    btn_clear.grid(row=2, column=0, padx=3, pady=3, columnspan=4, sticky="w")


    # This function will be called each time the user releases a key.
    def calculate(event):
        """Compute and display the user's slowest
        and fastest beneficial heart rates.
        """
        try:
            # Get the input in KG
            kg = ent_kg.get()

            # Compute the weight in LBs.
            lbs = kg * 2.204623

            # Display the weight.
            lbl_lbs_out.config(text=f"{lbs:.4f}")

        except ValueError:
            # When the user deletes all the digits in the KG entry box, clear the output label.
            lbl_lbs_out.config(text="")



    # This function will be called each time
    # the user presses the "Clear" button.
    def clear():
        """Clear all the inputs and outputs."""
        btn_clear.focus()
        ent_kg.clear()
        lbl_lbs_out.config(text="")
        ent_kg.focus()


    ent_kg.bind("<KeyRelease>", calculate)

    # Bind the clear function to the clear button so
    # that the computer will call the clear function
    # when the user clicks the clear button.
    btn_clear.config(command=clear)

    # Give the keyboard focus to the entry box.
    ent_kg.focus()


if __name__ == "__main__":
    main()
